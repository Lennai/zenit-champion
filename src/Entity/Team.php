<?php

namespace App\Entity;

class Team
{
    private string $name;
    private string $country;
    private string $logo;
    /**
     * @var Player[]
     */
    private array $players;
    private string $coach;
    private int $goals;

    public function __construct(string $name, string $country, string $logo, array $players, string $coach)
    {
        $this->assertCorrectPlayers($players);

        $this->name = $name;
        $this->country = $country;
        $this->logo = $logo;
        $this->players = $players;
        $this->coach = $coach;
        $this->goals = 0;
        $this->goalkeeperTime = 0;
        $this->defTime = 0;
        $this->midTime = 0;
        $this->forwardTime = 0;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getCountry(): string
    {
        return $this->country;
    }

    public function getLogo(): string
    {
        return $this->logo;
    }

    /**
     * @return Player[]
     */
    public function getPlayersOnField(): array
    {
        return array_filter($this->players, function (Player $player) {
            return $player->isPlay();
        });
    }

    public function getPlayers(): array
    {
        return $this->players;
    }

    public function getPlayer(int $number): Player
    {
        foreach ($this->players as $player) {
            if ($player->getNumber() === $number) {
                return $player;
            }
        }

        throw new \Exception(
            sprintf(
                'Player with number "%d" not play in team "%s".',
                $number,
                $this->name
            )
        );
    }

    public function getCoach(): string
    {
        return $this->coach;
    }

    public function addGoal(): void
    {
        $this->goals += 1;
    }

    public function getGoals(): int
    {
        return $this->goals;
    }

    public function setPositionTime(): void
    {
        foreach ($this->players as $player) {
            if ($player->getPosition() === "В") {
                $this->goalkeeperTime += $player->getPlayTime();
            }
            if ($player->getPosition() === "З") {
                $this->defTime += $player->getPlayTime();
            }
            if ($player->getPosition() === "П") {
                $this->midTime += $player->getPlayTime();
            }
            if ($player->getPosition() === "Н") {
                $this->forwardTime += $player->getPlayTime();
            }
        }
    }

    public function getGoalkeeperTime(): int
    {
        return $this->goalkeeperTime;
    }

    public function getDefTime(): int
    {
        return $this->defTime;
    }

    public function getMidTime(): int
    {
        return $this->midTime;
    }

    public function getForwardTime(): int
    {
        return $this->forwardTime;
    }

    private function assertCorrectPlayers(array $players)
    {
        foreach ($players as $player) {
            if (!($player instanceof Player)) {
                throw new \Exception(
                    sprintf(
                        'Player should be instance of "%s". "%s" given.',
                        Player::class,
                        get_class($player)
                    )
                );
            }
        }
    }
}